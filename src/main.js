import Vue from 'vue'
import App from './App.vue'
import AtComponents from 'at-ui'
import 'at-ui-style'    // Import CSS
import axios from 'axios'
import VueAxios from 'vue-axios'
import Ws from '@adonisjs/websocket-client/index'


Vue.config.productionTip = false
Vue.use(AtComponents)
Vue.use(VueAxios, axios)


Vue.prototype.$baseURL = "http://localhost:3333/"
Vue.prototype.$ws = Ws("ws://localhost:3333").connect()

new Vue({
  render: h => h(App),

}).$mount('#app')
